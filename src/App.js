import logo from './logo.svg';
import './App.css';
import Navbar from './pages/Navbar';

import {
  BrowserRouter as Router,
  Route,
  Switch,
  Redirect
} from "react-router-dom";
import Admin from './pages/Admin'
import User from './pages/User'
function App() {
  const defaultRoute = window.location.pathname === "/" ? <Redirect to="/home" /> : undefined;

  
  return (
    <Router>
      <Switch>
          <Route path='/admin' component={Admin} />
          <Route path='/user' component={User} />
      </Switch>
      
      {defaultRoute}
    </Router>
   );
}

export default App;
