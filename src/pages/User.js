import React from "react";
import axiosInstance from 'axios'
import { DataGrid } from '@material-ui/data-grid';
import { TabPanel, AppB } from '@material-ui/lab';
import { makeStyles } from "@material-ui/core";
import './User.css';
import {Button, Container, TextField} from  '@material-ui/core';
import Navbar from "./Navbar";


const columns = [
    {field: "id", headerName: "Id", width: 100 },
    {field: "name", headerName: "Name", width: 200 },
    {field: "age", headerName: "Age", width: 200 },
    {field: "email", headerName: "Email", width: 200 },
    {field: "password", headerName: "Password", width: 200 }
]
export default class User extends React.Component{
    constructor (){
        super();
        this.state={
            foods : [],
            id : 0,
            id2: "",
            id3: 0,
            id4: "",
            id5: ""

        };
        
        this.onSubmitDelete = this.onSubmitDelete.bind(this);
        this.onSubmitF = this.onSubmitF.bind(this);
        this.onSubmitEdit = this.onSubmitEdit.bind(this);
        axiosInstance
        .get("http://localhost:8080/user/getUser")
        .then(response => {
            const val = response.data
            this.setState({foods: val})
            console.log("mancare")
            console.log(val)
        })
        .catch(error =>{
            console.log(error);
        })
    }
    handleInput = event => {

        const { value,name } = event.target;
        this.setState(
            {
                [name]: value
            }
        );
        console.log(value);
    };
   
   

    onSubmitF(){

        let name = this.state.id2
        let age = this.state.id3
        let email = this.state.id4
        let pass = this.state.id5
    
        console.log( name)

        axiosInstance.post("http://localhost:8080/admin/addAdmin", {
            name: name,
            age: age,
            email: email,
            password: pass
          })
          .then(function (response) {
            console.log(response);
          })

           axiosInstance
        .get("http://localhost:8080/admin/getAdmin")
        .then(response => {
            const val = response.data
            this.setState({foods: val})
            console.log("mancare")
            console.log(val)
        })
        .catch(error =>{
            console.log(error);
        })

        
    }

    onSubmitEdit(){
        let delRes = this.state.id
        let name = this.state.id2
        let age = this.state.id3
        let email = this.state.id4
        let pass = this.state.id5

        console.log( name)

        axiosInstance.put("http://localhost:8080/admin/updateAdmin/" + delRes, {
            name: name,
            age: age,
            email: email,
            password: pass
          })
          .then(function (response) {
            console.log(response);
          })
    }

    onSubmitDelete(){

        let delRes = this.state.id
    
        console.log( delRes)
    
        axiosInstance.delete("http://localhost:8080/admin/deleteAdmin/" + delRes)
        .then(console.log("ok del"))
        .catch(error =>{
            console.log(error)
        })
    
    }


    render (){
        return (
            <div >
                <Navbar/>

                <div style={{display: 'flex',  justifyContent:'center', alignItems:'center'}}>
                    <div style={{ display: 'flex', height: 600,width:1750 }}>
                        <div   style={{ flexGrow: 5 , borderColor: '#1890ff', backgroundColor:'#f2f2f2'}}>
                            <DataGrid className='primary' rows={this.state.foods} columns={columns} pageSize={20}/>
                        </div>

                    </div>

                </div>

                <div style={{display: 'grid',marginTop:'50px',  justifyContent:'center', alignItems:'center'}}>
                
            
                    <div>
                
                    <TextField
                                style={{'margin-right':'70px'}}
                                id="standard-basic2"
                                label="name"
                                onChange = {this.handleInput}
                                name="id2" />

                    <TextField
                                style={{'margin-right':'70px'}}
                                id="standard-basic3"
                                label="age"
                                onChange = {this.handleInput}
                                name="id3" />

                    <TextField
                                style={{'margin-right':'70px'}}
                                id="standard-basic4"
                                label="email"
                                onChange = {this.handleInput}
                                name="id4" />

                    <TextField
                                style={{'margin-right':'70px'}}
                                id="standard-basic5"
                                label="password"
                                onChange = {this.handleInput}
                                name="id5" />         
                                
                    </div>
                    
                    <div  style = {{marginTop:'50px', marginLeft: '370px'}}>
                            <Button 
                                    class= 'button1'
                                    style = {{marginRight : '10px'}}
                                    onClick={this.onSubmitF}
                                    type = "submit"
                                    variant="contained"
                                     >
                                Add
                            </Button>
                      

                      
                            <Button
                                 class= 'button1'
                                    style = {{marginRight : '10px'}}
                                    onClick={this.onSubmitEdit}
                                    type = "submit"
                                    variant="contained"
                                    color="inherit" >
                                Edit
                            </Button>
                        

                       
                            <Button
                                    class= 'button1'   
                                    style = {{marginRight : '10px'}}
                                    onClick={this.onSubmitDelete}
                                    type = "submit"
                                    variant="contained"
                                    color="secondary" >
                                Delete
                            </Button>
                        </div>
                    </div>
                    <div>
                    <div style={{display: 'flex',  justifyContent:'center', alignItems:'center'}}>
                    <TextField
                            style={{'margin-right':'70px'}}
                            id="standard-basic"
                            label="Id"
                            onChange = {this.handleInput}
                            name="id" />
                    </div>
                    </div>
            </div>
        );
        
    }
 

}
